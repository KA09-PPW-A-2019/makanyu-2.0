from django.shortcuts import render, redirect
from menu.models import Pesan, MenuDipesan, Menu
from . import forms
from pesan_makanan.forms import PesanMakananForm
from pesan_makanan.models import PesanMakanan

def antri(request):
    listPesanan = PesanMakanan.objects.all()
    # for pesanan in listPesanan:
    #     print(pesanan.nama)
    #     print(pesanan.pesanan)
    
    return render(request, 'antri.html', {'antri' : listPesanan})