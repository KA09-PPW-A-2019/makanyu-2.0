from django.test import SimpleTestCase, TestCase, Client
from django.urls import reverse, resolve
from django.core.files.uploadedfile import SimpleUploadedFile
from .views import antri
# Create your tests here.
class FormHTMLTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.antrian_url = reverse('antrian:antri')
        
    def test_text_exist(self):
        response = self.client.get(self.antrian_url)
        self.assertIn('ANTRIAN', response.content.decode())
        
class TestUrls(SimpleTestCase):
    def test_antrian_url_is_resolved(self):
        url = reverse('antrian:antri')
        self.assertEquals(resolve(url).func, antri)