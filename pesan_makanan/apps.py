from django.apps import AppConfig


class PesanMakananConfig(AppConfig):
    name = 'pesan_makanan'
