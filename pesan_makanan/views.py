from django.shortcuts import render, redirect
from .models import PesanMakanan
from menu.models import Pesan
from . import forms

# Create your views here.
def pesan_makanan(request):
	total = 0
	pesanan_menu = Pesan.objects.last() #pesanMakanan ubah dengan hasilnya 
	for pesanan in pesanan_menu.menu.all():
		total += pesanan.menu.price * pesanan.amount
	
	if request.method == 'POST':
		pesan_makanan = forms.PesanMakananForm(request.POST)
		if pesan_makanan.is_valid():
			PesanMakanan(nama=pesan_makanan.cleaned_data['nama'], 
				jumlah_harga=total, 
				pesanan=Pesan.objects.last()).save()
			return redirect('antrian:antri')
	else:
		print(request.method)
		pesan_makanan = forms.PesanMakananForm()
	
	return render(request, 'pesanMakanan.html', {'title': 'CEK PESANAN', 'menu': pesanan_menu, 'pesan_makanan': pesan_makanan, 'total': total}) 