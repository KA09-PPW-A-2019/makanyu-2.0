from django.shortcuts import render, redirect, Http404, get_object_or_404
from .models import Menu, Pesan, MenuDipesan
from django.urls import reverse
from . import forms
# Create your views here.

def menumakan(request):
	if request.method == 'POST':
		form_pesanan = forms.PesanForm(request.POST)
		if form_pesanan.is_valid():
			pesanan = Pesan()
			pesanan.save()
			counter = 0
			for key, value in form_pesanan.cleaned_data.items():
				if key[:5] == 'menu_' and value > 0:
					counter += 1
					menu_dipesan = MenuDipesan(menu=Menu.objects.get(id=int(key[5:])),
						amount=value)
					menu_dipesan.save()
					pesanan.menu.add(menu_dipesan)
			if counter != 0:
				return redirect(reverse('pesan_makanan:pesan_makanan'))
	else:
		form_pesanan = forms.PesanForm()

	semua_menu = Menu.objects.all()
	for menu in semua_menu:
		menu.form_field = form_pesanan['menu_' + str(menu.id)]

	return render(request, 'menumakan.html', {'menus':semua_menu, 'form_pesanan': form_pesanan})

def menu_create(request):
    form = forms.MenuForm(request.POST, request.FILES) 
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('menu:menumakan')
    else:
        form = forms.MenuForm()
    return render(request, 'menu_create.html', {'form': form})


def search_menu(request):
    if request.is_ajax():
        filtered = Menu.objects.filter(name__icontains=request.GET.get('q', ''))
        data = serializers.serialize('json', filtered)
        return HttpResponse(data, content_type='application/json')
    raise Http404()
