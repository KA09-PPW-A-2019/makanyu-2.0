from django.test import TestCase, Client
from menu.forms import MenuForm, PesanForm
from menu.models import Pesan, MenuDipesan, Menu
from .models import Menu
from django.core.files.uploadedfile import SimpleUploadedFile

# Create your tests here.
class MenuHTMLTest(TestCase):
    def test_form(self):
        response = self.client.get('/menu/create/')
        
        self.assertIn('<form', response.content.decode())
        self.assertIn('<input', response.content.decode())

    def test_menu_url_is_exist(self):
        response = self.client.get('/menu/menumakan/')

        self.assertEqual(response.status_code,200)

    def test_cek_title(self):
        response = self.client.get('/menu/menumakan/')

        self.assertContains(response, 'Ayam Penyet Strike')

class MenuTest(TestCase):
    def test_setup_menu(self):
        foto = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        menu1 = {
            'name':'Ayam Taliwang',
            'price':22000,
            'image':SimpleUploadedFile('foto.gif', foto, content_type='image/gif'),
        }
        response=self.client.post('/menu/create/', menu1)
        self.assertEqual(response.status_code, 302)

    def test_forms_input_valid(self):
        response = self.client.get('/menu/create/')
        self.assertEqual(response.status_code, 200)

    def test_response_landing_page(self):
        response = Client().get('/menu/menumakan/')
        self.assertEqual(response.status_code, 200)

    def test_pesan_makan(self):
        Menu.objects.create(name='Ayam Bakar', price=17000, image=None)
        pesanan = Pesan()
        pesanan.save()
        data = { 'menu_1' : "3"}
        response =self.client.post('/menu/menumakan/', data)
        self.assertEqual(response.status_code, 302)

    def test_ajax_only(self):
        response = self.client.get('/menu/ajax_search/')
        self.assertEqual(response.status_code, 404)

    def test_search(self):
        headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        response = self.client.get('/menu/ajax_search?q=match', **headers)

        # self.assertContains(response, 'Match')
