from django.urls import path
from . import views

app_name = 'menu'

urlpatterns = [
    path('menumakan/', views.menumakan, name='menumakan'),
    path('create/', views.menu_create, name='menu_create'),
    path('ajax_search/', views.search_menu, name='ajax_search')
    ]
