var search_cache = {}

$('input[name=q]').keydown(function() {
  window.setTimeout(function() {
    var query = $('input[name=q]').val()
    if (query !== "") {
      $('div.menu').css('display', 'none')
      $('div.menu.search').css('display', 'block')
      if (!(query in search_cache)) {
        ajax_search(query)
      } else {
        $('div.menu.search').html(search_cache[query])
      }
    } else {
      $('div.menu').css('display', 'block')
      $('div.menu.search').css('display', 'none')
    }
  }, 200)
})

function ajax_search(query) {
  var html = ""
  $.ajax({
    url: search_url + "?q=" + query,
    type: "GET",
    success: function(result) {
      console.log(result)
      html = `
<h1>Result</h1>
<div class="container-fluid">
          `
      if (result.length !== 0) {
        $.each(result, function(i) {
          var menu = result[i].fields
          html += `
  <div class="row align-items-center">
    <div class="col-6 col-md-2">
      <span class="name">` + menumakan.name + `</span>
      <br>
      <span class="price">` + (menumakan.price) + `</span>
    </div>
    <div class="col-6 col-md-2">
      <img src="/media/` + menumakan.image + `">
    </div>
    <div class="col-6 col-md-3">
      <div class="input-number position-relative">
        `
          if (authenticated === true) {
          html += `
          <input type="number" name="menu_` + result[i].pk + `" value="0" min="0" max="32767" required="" id="id_menu_` + result[i].pk 
          `
          }
        html += `
      </div>
    </div>
  </div>
        `
        })
      } else {
        html += 'Menu not found'
      }
      html += `
</div>
          `
      $('div.menu.search').html(html)
      search_cache[query] = html
    },
    error: function(a, b) {
      console.log(a)
      console.log(b)
    }
  })
}
