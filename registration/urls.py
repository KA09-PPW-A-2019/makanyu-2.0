from django.conf.urls import url
from registration import views

from django.urls import path

app_name = 'registration'

urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
]