from django.test import SimpleTestCase, TestCase, Client
from django.urls import reverse, resolve
from django.core.files.uploadedfile import SimpleUploadedFile

from .views import home

from .forms import RestaurantForm
from .models import Restaurant


# Create your tests here.

# Testing HTML
class FormHTMLTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.homepage_url = reverse('homepage:home')
        
    def test_text_exist(self):
        response = self.client.get(self.homepage_url)
        self.assertIn('Mengantri <br> Memang Menyebalkan.', response.content.decode())

    def test_form(self):
        response = self.client.get(self.homepage_url)
        self.assertContains(response, '<button')
        

# Testing URLs
class TestUrls(SimpleTestCase):
    def test_home_url_is_resolved(self):
        url = reverse('homepage:home')
        self.assertEquals(resolve(url).func, home)
        
        
# Testing Views
class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.homepage_url = reverse('homepage:home')
    
    def test_status_GET(self):
        response = self.client.get(self.homepage_url)
        
        self.assertEquals(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)
        self.assertTemplateUsed(response, 'home.html', 'base.html')
        
    def test_status_GET_wrong_url(self):
        url = '/kwek'
        response = self.client.get(url)
        
        self.assertEqual(response.status_code, 404)

    def test_restaurant_form(self):
        foto = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )


        request = {'nama' : "Ayam Geprek",
             'deskripsi' : "Ayam Terprekgeprek",
             'alamat' : "Jl Menuju Surga",
             'gambar' : SimpleUploadedFile('foto.gif', foto, content_type='image/gif'),
             }
        response = self.client.post(self.homepage_url, data = request)
        self.assertEqual(response.status_code, 302)
        
        
# Testing Models
class RestaurantViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.homepage_url = reverse('homepage:home')
        
    def test_form_valid(self):
        foto = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )

        
        response = Restaurant(nama = 'Ayam Geprek',
             deskripsi = 'Ayam Terprekgeprek',
             alamat = 'Jl Menuju Surga',
             gambar = SimpleUploadedFile('foto.gif', foto, content_type='image/gif'),
        )

        response.save()
        
        self.assertEqual(Restaurant.objects.all().count(), 1)
        self.assertTrue(Restaurant.objects.filter(nama = "Ayam Geprek").exists())
