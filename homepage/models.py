from django.db import models

# Create your models here.

class Restaurant(models.Model):
    nama = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=50)
    alamat = models.CharField(max_length=50)
    gambar = models.ImageField(upload_to='images/')

        
