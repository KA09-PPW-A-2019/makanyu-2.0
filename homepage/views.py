from django.shortcuts import render, redirect
from .forms import RestaurantForm
from .models import Restaurant

# Create your views here.


def home(request):
    if request.method == 'POST':
        form = RestaurantForm(request.POST,request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/home')
    else:
        form = RestaurantForm()


    temp = Restaurant.objects.all()
    
    return render(request, 'home.html', {'restaurant_list': temp,
                                         'ini_restaurant_form': form,
                                         'home_page' : True,})
